class dovecot::debian inherits dovecot::base {

  # install everything from dovecot::base until this can be rewritten
  # Package['dovecot'] { name => [ 'dovecot-common', 'dovecot-imapd', 'dovecot-pop3d' ] }

  File['/etc/dovecot.conf'] { path => '/etc/dovecot/dovecot.conf' }

}

