class dovecot(
  $sqlite = false,
  $pgsql = false,
  $mysql = false,
  $munin_checks = true,
  $manage_shorewall = true,
  $version = 2
){

  include dovecot::params

  case $operatingsystem {
    centos: { include dovecot::centos }
    debian: { include dovecot::debian }
    default: { include dovecot::base }
  }

  if $dovecot::sqlite or $dovecot::pgsql or $dovecot::mysql {
    include dovecot::sql
  }

  if $dovecot::manage_shorewall {
    include shorewall::rules::pop3
    include shorewall::rules::imap
    if $dovecot::params::type == 'proxy' {
      include shorewall::rules::out::imap
      include shorewall::rules::out::pop3
    }
  }

  if $dovecot::munin_checks {
    include dovecot::munin
  }
}
