class dovecot::deliver {

  include ::dovecot
  include dovecot::params
  
  file { [ '/var/log/dovecot/deliver.log',
        '/var/log/dovecot/deliver-error.log' ]:
          require => Package[$dovecot::params::dovecot_software],
          before  => Service['dovecot'],
          owner   => root,
          group   => 12,
          mode    => '0660';
  }
}
