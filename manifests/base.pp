class dovecot::base {

  include dovecot::params

  package { $dovecot::params::dovecot_software:
    ensure => installed
  }

  file { '/etc/dovecot.conf':
    source  => [ "puppet:///modules/site_dovecot/config/${::fqdn}/dovecot.conf",
                 "puppet:///modules/site_dovecot/config/${$dovecot::params::type}/dovecot.conf",
                 'puppet:///modules/site_dovecot/config/dovecot.conf',
                 "puppet:///modules/dovecot/config/${::operatingsystem}/dovecot.conf",
                 'puppet:///modules/dovecot/config/dovecot.conf' ],
    require => Package[$dovecot::params::dovecot_software],
    notify  => Service['dovecot'],
    owner   => root,
    group   => mail,
    mode    => '0640';
  }

  file { 'dovecot_config_dir':
    ensure  => directory,
    path    => '/etc/dovecot/conf.d',
    require => Package[$dovecot::params::dovecot_software],
    owner   => dovecot,
    group   => 0,
    mode    => '0755';
  }

  file {
    '/var/log/dovecot':
      ensure  => directory,
      require => Package[$dovecot::params::dovecot_software],
      before  => Service['dovecot'],
      owner   => dovecot,
      group   => dovecot,
      mode    => '0750';

    [ '/var/log/dovecot/error.log',
      '/var/log/dovecot/dovecot.log' ]:
        require => Package[$dovecot::params::dovecot_software],
        before  => Service['dovecot'],
        owner   => root,
        group   => dovecot,
        mode    => '0660';
  }

  service { 'dovecot':
    ensure => running,
    enable => true,
  }
}
