class dovecot::params {
  $manage_shorewall = true
  $munin_checks = true
  $type = 'some_unknown_type'
  $dovecot_software = [ 'dovecot-core', 'dovecot-imapd', 'dovecot-pop3d', 'dovecot-mysql' ]
}
