class dovecot::expire ( $type = 'sqlite', $mail_location = '', $dirs = '', $days = '' ) {

  file { '/etc/cron.daily/dovecot-expire':
    owner => root,
    group => 0,
    mode  => '0755';
  }

  if $type == 'legacy' or $type == 'mixed' {
    if $mail_location == '' {
      fail("Need to set \$dovecot_mail_location on ${::fqdn}!")
    }
    if $dirs == '' {
      $dirs = 'Trash\|Junk'
    }
    if $days == '' {
      $days = '14'
    }
    File['/etc/cron.daily/dovecot-expire']{
      content => "find ${mail_location} -regex '.*/\\.\\(${dirs}\\)\\(/.*\\)?\\/\\(cur\\|new\\)/.*' -type f -ctime +${days} -delete\n"
    }
  } else {
    # dovecot version 2 way (no mail_location, dirs need to be space separated variables and expire script runs doveadm expunge)
    # problem with this method is that it doesn't allow for different times for different mailboxes
    if $dirs == '' {
      $dirs = 'Trash Junk'
    }
    if $days == '' {
      $days = '14'
    }
    File['/etc/cron.daily/dovecot-expire']{
      content => "#!/bin/sh\n\n dirs='${dirs}'\nfor mailbox in \$dirs; do doveadm expunge -A mailbox \$mailbox savedbefore ${days}d; done\n"
    }
  }

  case $type {
    'legacy': { info('no need to include anything for legacy mode') }
    'mixed': { include ::dovecot::expire::sqlite }
    'mysql': { include ::dovecot::expire::mysql }
    default: { include ::dovecot::expire::sqlite }
  }
}
