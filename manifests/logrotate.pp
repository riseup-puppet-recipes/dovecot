class dovecot::logrotate {

  augeas {
    'logrotate_dovecot':
      changes => [ 'rm /files/etc/logrotate.d/dovecot/rule/file',
                   'ins file before /files/etc/logrotate.d/dovecot/rule/*[1]',
                   'ins file before /files/etc/logrotate.d/dovecot/rule/*[1]',
                   'set /files/etc/logrotate.d/dovecot/rule/file[1] /var/log/dovecot/dovecot.log',
                   'set /files/etc/logrotate.d/dovecot/rule/file[2] /var/log/dovecot-lda.log',
                   'set rotate 3', 'set schedule daily', 'set compress compress', 'set sharedscripts sharedscripts',
                   'set create/mode 0660', 'set create/owner dovecot', 'set create/group dovecot',
                   'set postrotate "/bin/kill -USR1 `cat /var/run/dovecot/master.pid 2>/dev/null` 2> /dev/null || true"' ],
  }
}
