class dovecot::munin {

  munin::plugin::deploy { 'dovecot':
    source => 'dovecot/munin/dovecot',
    config => "env.logfile /var/log/dovecot/dovecot.log\nuser root"
  }

  munin::plugin::deploy { "dovecot_stats_${::domain}":
    source => 'dovecot/munin/dovecot_stats_',
    config => 'user root'
  }
}
