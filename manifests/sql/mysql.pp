class dovecot::sql::mysql {

  if $version == 2 {
    package { 'dovecot-mysql':
      ensure => installed,
      before => [ File['/etc/dovecot-sql.conf'], File['/etc/dovecot-dict-sql.conf'] ];
    }
  }
}
