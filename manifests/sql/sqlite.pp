class dovecot::sql::sqlite {

  if $version == 2 {
    package { 'dovecot-sqlite':
      ensure => installed,
      before => File['/etc/dovecot-sql.conf'],
    }
  }
}
