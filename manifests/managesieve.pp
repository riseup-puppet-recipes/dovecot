class dovecot::managesieve(
  $type = 'some_unknown_type',
  $manage_shorewall = true,
) {

  $managesieve_package = $operatingsystem ? {
    debian => managesieved,
    default => managesieve
  }

  package { $managesieve_package:
    ensure => installed,
    before => Service['dovecot'],
  }

  if $dovecot::managesieve::manage_shorewall {
    include shorewall::rules::managesieve
    if $dovecot::managesieve::type == 'proxy' {
      include shorewall::rules::out::managesieve
    }
  }
}
