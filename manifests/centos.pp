class dovecot::centos inherits dovecot::base {

  include dovecot::params

  file { '/etc/sysconfig/dovecot':
    source  => [ "puppet:///modules/site_dovecot/sysconfig/${::fqdn}/dovecot",
                 "puppet:///modules/site_dovecot/sysconfig/${dovecot::params::type}/dovecot",
                 'puppet:///modules/site_dovecot/sysconfig/dovecot',
                 'puppet:///modules/dovecot/sysconfig/dovecot' ],
    require => Package[$dovecot::params::dovecot_software],
    notify  => Service['dovecot'],
    owner   => root,
    group   => mail,
    mode    => '0640';
  }
}
