# for sequential sieve scripts, this one is the after_sieve
# keep is implicit, but it seems that only explicit fileinto actions 
# are logged and not the implicit keep action, so we specify it explicitly
fileinto "INBOX";
stop;
